﻿Shader "Custom/CullFrontShader"
{

	Properties{
	_MainTex("Texture", 2D) = "black" { }
	}

		SubShader{

	  Tags { "RenderType" = "Opaque" }

	  Cull Front

	  CGPROGRAM

	  #pragma surface surf Lambert vertex:vert

	  void vert(inout appdata_full v)
	  {
		  v.normal.xyz = v.normal*-1;
	  }

	  struct Input {
		  //float4 color : COLOR;
		  float2 uv_MainTex;
	  };

	  sampler2D _MainTex;

	  void surf(Input IN, inout SurfaceOutput o) {
		  o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
		  //o.Metallic = 0.2;
		  //o.Alpha = 1;
	  }

	  ENDCG

	}

		Fallback "Diffuse"
}
