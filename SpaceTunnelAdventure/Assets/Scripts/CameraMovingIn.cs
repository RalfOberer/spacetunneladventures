﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovingIn : MonoBehaviour
{
    public GameObject targetPostion; //VR Handler or Camera or whatever.. atm maincamera child of set
                                     //public GameObject speedOfPlayerSet;



    private bool cameraDone = false;

    public bool CameraDone { get => cameraDone; set => cameraDone = value; }


    // Start is called before the first frame update
    void Start()
    {
        cameraDone = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!cameraDone)
        {
            Rigidbody rigidbody = GetComponent<Rigidbody>();
            Rigidbody rigidbodyTarget = targetPostion.GetComponent<Rigidbody>();
            //float playerSpeed = speedOfPlayerSet.GetComponent<AutomaticPlayerSetControl>().DifficultySpeed;


            rigidbody.transform.position = Vector3.MoveTowards(rigidbody.transform.position, rigidbodyTarget.transform.position, 0.08f);

            if (Vector3.Distance(rigidbody.transform.position, rigidbodyTarget.transform.position) < 0.1f)
            {
                CameraDone = true;
                targetPostion.SetActive(true);
                //GetComponent<GameObject>().SetActive(false);
                


    /*
     * Wichtig hier ist der VirtualRealityController active zu schalten
     * */



}
        }


    }

    /*  private void Update()
      {
          Rigidbody rigidbody = GetComponent<Rigidbody>();
          Rigidbody rigidbodyTarget = targetPostion.GetComponent<Rigidbody>();
          //float playerSpeed = speedOfPlayerSet.GetComponent<AutomaticPlayerSetControl>().DifficultySpeed;


          rigidbody.transform.position = Vector3.MoveTowards(rigidbody.transform.position, rigidbodyTarget.transform.position,  0.05f);
      }*/


}
