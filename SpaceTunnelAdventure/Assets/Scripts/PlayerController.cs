﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour
{

    //Collision Variables
    public GameObject explosionEffect;
    private int playerHP = 3;
    private bool dead = false;
    private bool paused = true;
    //private bool tempdead = false;
    private System.Random random = new System.Random();

    public bool Dead { get => dead; set => dead = value; }
    public int PlayerHP { get => playerHP; set => playerHP = value; }
    public bool Paused { get => paused; set => paused = value; }



    //von obi
    private GameObject m_NodeToMove = null;


    //Movement Variable Department 
    private float difficultySpeed = 30.0f;
    private float hardestDifficultyLevel = 60.0f;
    //public GameObject movingInCamera;
    //public Boundary boundary;
    private float highscore = 0.0f;

    public bool vrControlActive = true;
    private float hardModeFactor = 1.0f;


    public float Highscore { get => highscore; set => highscore = value; }
    public float DifficultySpeed { get => difficultySpeed; set => difficultySpeed = value; }
    public float HardestDifficultyLevel { get => hardestDifficultyLevel; set => hardestDifficultyLevel = value; }
    public float HardModeFactor { get => hardModeFactor; set => hardModeFactor = value; }


    IEnumerator OnTriggerEnter(Collider other)
    {
        

        if (other.gameObject.name.Equals("AsteroidF1(Clone)"))
        {
            Rigidbody rigidbody = GetComponent<Rigidbody>();
            Rigidbody rigidbodyAsteroid = other.GetComponent<Rigidbody>();

            //Explosion on Damage taken
            Vector3 spawnPointExplosion = new Vector3(rigidbodyAsteroid.transform.position.x, rigidbodyAsteroid.transform.position.y, rigidbody.transform.position.z + 8);
            GameObject explosion = Instantiate(explosionEffect, spawnPointExplosion, rigidbody.transform.rotation);

            yield return new WaitForSeconds(0.1f);

            //Damage taken
            PlayerHP--;
            //tempdead = false;

            //Player dead?
            if (PlayerHP <= 0)
            {
                Dead = true;
                //tempdead = true;
            }



            //if (tempdead)
            //{
                /*Vector3 movement = new Vector3(0.0f, 0.0f, 0.0f);
                rigidbody.velocity = movement;
                for (int i = 2; i > -2; i--)
                {
                    int shiftExplosion = random.Next(-1, 1);
                    //collisionEffectAsteroids.GetComponentInChildren<Renderer>().
                    Debug.Log("Entered Explosions");
                    //Vector3 spawnPointDeathExplosion = new Vector3(rigidbody.transform.position.x + (i*shiftExplosion), rigidbody.transform.position.y + (i * shiftExplosion), rigidbody.transform.position.z + 5);
                    Vector3 spawnPointDeathExplosion = new Vector3(rigidbody.transform.position.x - (i * shiftExplosion), rigidbody.transform.position.y + (i * shiftExplosion), rigidbody.transform.position.z*  + 8);
                    GameObject deathExplosion = Instantiate(explosionEffect, spawnPointDeathExplosion, rigidbody.transform.rotation);
                    yield return new WaitForSeconds(0.2f);
                }*/
              //  Dead = true;
           // }
            Destroy(other.gameObject.GetComponent<Renderer>());
        }



    }




    void Start()
    {
        //tempdead = false;
        paused = true;
        Dead = false;
    }

    // Update is called once per frame
    void Update()
    {

        Rigidbody rigidbody = GetComponent<Rigidbody>();

        if (!dead && !Paused)
        {
            float movementHorizontal = 0.0f;
            if (vrControlActive)
            {
                string NodeToMove = "HandNode";
                m_NodeToMove = GameObject.Find(NodeToMove);

                float roundedtrans = m_NodeToMove.transform.localRotation.eulerAngles.y;



                if (roundedtrans >= 180 && roundedtrans < 360)
                {
                    roundedtrans = roundedtrans - 360;
                }



                movementHorizontal = (float)(roundedtrans / 100);

            }
            else
            {
                movementHorizontal = Input.GetAxis("Horizontal");
            }


            rigidbody.rotation = Quaternion.Euler(0.0f, 180.0f, movementHorizontal * 180.0f);

        }

    }

    void FixedUpdate()
    {
        if (!dead && !paused)
        {



            Vector3 movement = new Vector3(0.0f, 0.0f, 1.0f);
            Rigidbody rigidbody = GetComponent<Rigidbody>();
            rigidbody.velocity = movement * DifficultySpeed*hardModeFactor;
            Highscore = rigidbody.position.z;


        }
        else
        {
            Vector3 movement = new Vector3(0.0f, 0.0f, 0.0f);
            Rigidbody rigidbody = GetComponent<Rigidbody>();
            rigidbody.velocity = movement * DifficultySpeed * hardModeFactor;
        }
    }

}




