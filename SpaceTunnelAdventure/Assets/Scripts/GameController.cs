﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;
using MiddleVR_Unity3D;

public class GameController : MonoBehaviour
{

    private bool gameRunning = false;

    public VRWand m_Wand = null;

    public GameObject playerSet;
    public GameObject gameOverOverlay;
    public GameObject pauseOverlay;
    public GameObject startOverlay;
    public GameObject highscoreText;
    public GameObject explosionEffect;
    //private System.Random random = new System.Random();
    public GameObject stick;
    private bool firstStart = true;

    void Start()
    {
        //ResumeGame();
        PauseGame();
        //m_Wand = this.GetComponent<VRWand>();
        m_Wand.SetRayLength(0.02f);
   
        Debug.Log("work!");

    }




    void Update()
    {
        m_Wand.SetRayLength(0.15f);
        if (stick.GetComponent<HUD1>().exitApplication)
        {
            ExitGame();
            Debug.Log("updategamecontrollerscript");
        }

        if (playerSet.GetComponent<PlayerController>().Dead)
        {
            EndGame();
        }
        else
        {

            /*if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (gameRunning)
                {
                    pauseGame();
                }
                else
                {
                    resumeGame();
                }
            }*/

            /*if (lever.GetComponent<LeverController>().hardmode)
            {
                //lets maybe do smth...but mäh..maybe player
            }*/

            if (stick.GetComponent<HUD1>().paused && gameRunning)
            {
                PauseGame();
            }
            else if (!stick.GetComponent<HUD1>().paused && !gameRunning)
            {
                ResumeGame();
            }
            else
            {
                //Do nothing, everything is fine
            }

           






        }

    }

    private void EndGame()
    {

        m_Wand.SetRayLength(30.0f);
        int highscore = (int)(playerSet.GetComponent<PlayerController>().Highscore);
        highscoreText.GetComponent<Text>().text = "You reached a Score of : " + highscore + " Points";
        //collisionHPHandlerObject.GetComponent<CollisionDetectionPlayerBody>().

        Vector3 gameOverOverlayPosition = playerSet.GetComponent<Rigidbody>().position;
        gameOverOverlay.GetComponent<RectTransform>().position = new Vector3(gameOverOverlayPosition.x, gameOverOverlayPosition.y, gameOverOverlayPosition.z + 8);

        gameOverOverlay.SetActive(true);
        //Time.timeScale = 0;

        if (gameOverOverlay.GetComponent<GameOverController>().RestartGame)
        {
            RestartGame();
        }
                
            
     




    }



    public void PauseGame()
    {
        playerSet.GetComponent<PlayerController>().Paused = true;
        gameRunning = false;
        //Time.timeScale = 0;
        //pauseOverlay.SetActive(true);
        if (!pauseOverlay.active && !firstStart)
        {
            Vector3 pauseOverlayPosition = playerSet.GetComponent<Rigidbody>().position;
            pauseOverlay.GetComponent<RectTransform>().position = new Vector3(pauseOverlayPosition.x, pauseOverlayPosition.y, pauseOverlayPosition.z + 8);

            pauseOverlay.SetActive(true);
        }
    }


    public void ResumeGame()
    {
        if (firstStart)
        {
            //Set startoverlay inactive
            startOverlay.SetActive(false);
            firstStart = false;
        }

        gameRunning = true;
        playerSet.GetComponent<PlayerController>().Paused = false;
        //Time.timeScale = 1;
        pauseOverlay.SetActive(false);
        if (pauseOverlay.active)
        {
            pauseOverlay.SetActive(true);
        }
    }


    public void RestartGame()
    {
        //restart Scene
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);

    }

    public void ExitGame()
    {
        Application.Quit();
        //SceneManager.LoadScene("Menu");
    }


}
