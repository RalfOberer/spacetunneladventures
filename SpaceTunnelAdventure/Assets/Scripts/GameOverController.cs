﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverController : MonoBehaviour
{
    private bool restartGame = false;

    public bool RestartGame { get => restartGame; set => restartGame = value; }

    protected void OnMVRWandButtonReleased(VRSelection iSelection)
    {
            RestartGame = true;    
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
