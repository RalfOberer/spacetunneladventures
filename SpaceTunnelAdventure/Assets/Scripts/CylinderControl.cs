﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[System.Serializable]
public class Textures
{
    public Texture texture0, texture1, texture2, texture3, texture4, texture5, texture6, texture7;
}


public class CylinderControl : MonoBehaviour
{


    public GameObject asteroid;
    public GameObject player;
    public Textures textures;

    //OtherScripts
    public PlayerController movementControl;


    private float currentSpeedLevel = 0.0f;
    private System.Random randomCalculator = new System.Random();
    private List<GameObject> asteroids = new List<GameObject>();
    private float shiftTunnel;
    private int maxAsteroidCount = 50;
    private float safespace = 15.0f;
    private List<Texture> textureList;

    public int MaxAsteroidCount { get => maxAsteroidCount; set => maxAsteroidCount = value; }



    void Start()
    {

        InstantiateAsteroids(50.0f);
        maxAsteroidCount = 50;

        textureList = new List<Texture>();
        textureList.Add(textures.texture0);
        textureList.Add(textures.texture1);
        textureList.Add(textures.texture2);
        textureList.Add(textures.texture3);
        textureList.Add(textures.texture4);
        textureList.Add(textures.texture5);
        textureList.Add(textures.texture6);
        textureList.Add(textures.texture7);

    }

    void InstantiateAsteroids(float newPositionZCylinder)
    {
        asteroids.Clear();
        for (int i = 2; i <= MaxAsteroidCount; i++)
        {
            Vector3 calculatedCoordsPosition = CalculateCoords(newPositionZCylinder, i);
            if (calculatedCoordsPosition.x == 0.0f && calculatedCoordsPosition.y == 0.0f && calculatedCoordsPosition.z == 0.0f)
            {
                GameObject shiftToTheEnd = asteroids[asteroids.Count - 1];
                foreach (GameObject ast in asteroids)
                {
                    if (shiftToTheEnd.GetComponent<Rigidbody>().transform.position.z < ast.GetComponent<Rigidbody>().transform.position.z)
                    {
                        shiftToTheEnd = ast;
                    }
                }
                asteroids.Remove(shiftToTheEnd);
                asteroids.Add(shiftToTheEnd);

                break;
            }
            else
            {
                GameObject asteroidPlaced = Instantiate(asteroid, calculatedCoordsPosition, asteroid.transform.rotation);
                asteroids.Add(asteroidPlaced);

            }

        }

    }



    Vector3 CalculateCoords(float newPositionZCylinder, int currentAsteroidCount)
    {


        Renderer renderer = GetComponent<Renderer>();
        Rigidbody rigidbody = GetComponent<Rigidbody>(); 


        Renderer rendererAsteroid = asteroid.GetComponent<Renderer>();
        Rigidbody rigidbodyAsteroid = asteroid.GetComponent<Rigidbody>(); 


        float radiusCylinderX = (renderer.bounds.max.x - rigidbody.position.x);
        float radiusCylinderY = (renderer.bounds.max.y - rigidbody.position.y);

        float radiusXAsteroid = (rendererAsteroid.bounds.max.x - rigidbodyAsteroid.position.x);
        float radiusYAsteroid = (rendererAsteroid.bounds.max.y - rigidbodyAsteroid.position.y);

        float cylinderMaxDepth = rigidbody.position.z + rigidbody.transform.localScale.y - safespace; //renderer.bounds.max.z;





        float angleDegree = (float)(randomCalculator.Next(0, 360));


        if (angleDegree > 90 && angleDegree <= 180)
        {
            radiusYAsteroid *= (-1);
        }
        else if (angleDegree > 180 && angleDegree <= 270)
        {
            radiusXAsteroid *= (-1);
            radiusYAsteroid *= (-1);
        }
        else if (angleDegree > 270 && angleDegree <= 360)
        {
            radiusXAsteroid *= (-1);
        }






        float angle = angleDegree * (Mathf.PI / 180.0f);

        float xCoordAsteroid = (Mathf.Sin(angle) * radiusCylinderX) - radiusXAsteroid;

        float yCoordAsteroid = (Mathf.Cos(angle) * radiusCylinderY) - radiusYAsteroid;




        float randomSpace = (float)randomCalculator.Next(31, 31);

        float newZCoordAsteroid = ((newPositionZCylinder - (shiftTunnel / 2))+safespace) + currentAsteroidCount * randomSpace;

        if (newZCoordAsteroid > cylinderMaxDepth)
        {
            return new Vector3(0.0f, 0.0f, 0.0f);
        }


        int randomSecondSpawn = randomCalculator.Next(0, 3);

        if (randomSecondSpawn == 1)
        {

            float angleAdditionDegree = (float)(randomCalculator.Next(40, 90));
            int signedOrNot = randomCalculator.Next(-1, 1);
            if (signedOrNot == 0) signedOrNot = 1;
            float actualAngleDeg = angleDegree + angleAdditionDegree * signedOrNot;

            if (actualAngleDeg < 0) actualAngleDeg *= (-1);


            radiusXAsteroid = (rendererAsteroid.bounds.max.x - rigidbodyAsteroid.position.x);
            radiusYAsteroid = (rendererAsteroid.bounds.max.y - rigidbodyAsteroid.position.y);
            if (actualAngleDeg > 90 && actualAngleDeg <= 180)
            {
                radiusYAsteroid *= (-1);
            }
            else if (actualAngleDeg > 180 && actualAngleDeg <= 270)
            {
                radiusXAsteroid *= (-1);
                radiusYAsteroid *= (-1);
            }
            else if (actualAngleDeg > 270 && actualAngleDeg <= 360)
            {
                radiusXAsteroid *= (-1);
            }


            float actualAngle = actualAngleDeg * (Mathf.PI / 180.0f);

            float xCoordAsteroid2 = (Mathf.Sin(actualAngle) * radiusCylinderX) - radiusXAsteroid;

            float yCoordAsteroid2 = (Mathf.Cos(actualAngle) * radiusCylinderY) - radiusYAsteroid;



            GameObject asteroidPlaced = Instantiate(asteroid, new Vector3(xCoordAsteroid2, yCoordAsteroid2, newZCoordAsteroid), asteroid.transform.rotation);
            asteroids.Add(asteroidPlaced);

        }


        return new Vector3(xCoordAsteroid, yCoordAsteroid, newZCoordAsteroid);
    }





    // Start is called before the first frame update
    void OnTriggerExit(Collider other)
    {

        if (other.gameObject.name.Equals("Player"))
        {
            
            if (currentSpeedLevel < movementControl.HardestDifficultyLevel)
            {
                movementControl.DifficultySpeed = ++currentSpeedLevel;
            }
            else
            {
                movementControl.DifficultySpeed = currentSpeedLevel;
            }

            Rigidbody rigidbody = GetComponent<Rigidbody>();
            shiftTunnel = rigidbody.transform.localScale.y * 2;
            float newZPosition = rigidbody.position.z + shiftTunnel;
            rigidbody.MovePosition(new Vector3(rigidbody.position.x, rigidbody.position.y, newZPosition));
            InstantiateAsteroids(newZPosition);

            int textureIndicator = randomCalculator.Next(0, 7);
            Renderer renderer = GetComponent<Renderer>();

            renderer.material.mainTexture = textureList[textureIndicator];


    }
        else if (other.gameObject.name.Equals("AsteroidF1(Clone)"))
        {
            Destroy(other.gameObject);
            // Debug.Log("TEst right MEthod entered");

        }
        else
        {
            //Debug.Log("Not correctly handled GameObject: " + other.gameObject.name);
        }


    }



    // Update is called once per frame
    void Update()
    {


    }

    private void FixedUpdate()
    {

        GameObject lastAsteroid = (GameObject)(asteroids[asteroids.Count - 1]);


        if (movementControl.DifficultySpeed != 100)
        {
            currentSpeedLevel = movementControl.DifficultySpeed;
        }


        if (player.GetComponent<Rigidbody>().transform.position.z > lastAsteroid.GetComponent<Rigidbody>().transform.position.z) 
        {
            movementControl.DifficultySpeed = 100;
        }
    }




}

