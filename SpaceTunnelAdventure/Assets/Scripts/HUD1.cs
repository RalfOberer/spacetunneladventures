﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD1 : MonoBehaviour
{
    private float t0 = 0.0f;
    public bool longClick;
    public bool shortClick;

    public bool paused = true;
    public bool exitApplication = false;

    protected void OnMVRWandButtonPressed(VRSelection iSelection)
    {
        //TImer
        t0 = Time.time;

        if (!paused)
        {
            paused = true;
        }
        else
        {
            paused = false;
        }
    }

    protected void OnMVRWandButtonReleased(VRSelection iSelection)
    {
        if ((Time.time - t0) > 5.4f)
        {
            exitApplication = true;
            Debug.Log("exit");
        }
        else
        {
            //Hier normalklick
        }
    }




    // Start is called before the first frame update
    void Start()
    {
        paused = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
